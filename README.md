# Required
    - installed Nodejs
    - installed ng cli
# How to run project in local
    - clone source code from repo git
    - cd folder source code
    - run : "npm i"
    - run : "ng serve"
    - app will start in browser at : http://localhost:4200/
# How to create a component
    - run : "ng g c components\component-name --skip-import"
    - import component to app.module.ts
    - import route to app-routing.module
